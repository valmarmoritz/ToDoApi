﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;
using System.Linq;

namespace TodoApi.Controllers
{
	[Route("api/todo")]
	public class TodoController : Controller
	{
		private readonly TodoContext _context;

		public TodoController(TodoContext context)
		{
			_context = context;

            if (_context.TodoItems.Count() == 0)
			{
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "jaluta koera" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "käi poes" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "korista oma tuba ära" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "õpi Angular selgeks" });
				_context.TodoItems.Add(new TodoItem { IsComplete = true, Name = "käi saunas'" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "otsi talvesaapad üles" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "vii vanaema Pärnusse" });
				_context.TodoItems.Add(new TodoItem { IsComplete = true, Name = "harjuta esinemist" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "uuenda oma LinkedIni profiili" });
				_context.TodoItems.Add(new TodoItem { IsComplete = false, Name = "pese mustad nõud ära" });
				_context.SaveChanges();
			}
		}

		// GET /api/todo
		[HttpGet]
		public IEnumerable<TodoItem> GetAll()
		{
			return _context.TodoItems.ToList();
		}

		// GET /api/todo/{id}
		[HttpGet("{id}", Name = "GetTodo")]
		public IActionResult GetById(long id)
		{
			var item = _context.TodoItems.FirstOrDefault(t => t.Id == id);
			if (item == null)
			{
				return NotFound();
			}
			return new ObjectResult(item);
		}


		[HttpPost]
		public IActionResult Create([FromBody] TodoItem item)
		{
			if (item == null)
			{
				return BadRequest();
			}

			_context.TodoItems.Add(item);
			_context.SaveChanges();

			return CreatedAtRoute("GetTodo", new { id = item.Id }, item);
		}

		// PUT /api/todo/{id}
		[HttpPut("{id}")]
		public IActionResult Update(long id, [FromBody] TodoItem item)
		{
			if (item == null || item.Id != id)
			{
				return BadRequest();
			}

			var todo = _context.TodoItems.FirstOrDefault(t => t.Id == id);
			if (todo == null)
			{
				return NotFound();
			}

			todo.IsComplete = item.IsComplete;
			todo.Name = item.Name;

			_context.TodoItems.Update(todo);
			_context.SaveChanges();
			return new NoContentResult();
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(long id)
		{
			var todo = _context.TodoItems.FirstOrDefault(t => t.Id == id);
			if (todo == null)
			{
				return NotFound();
			}

			_context.TodoItems.Remove(todo);
			_context.SaveChanges();
			return new NoContentResult();
		}
	}
}